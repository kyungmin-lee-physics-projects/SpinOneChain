using DrWatson
@quickactivate "SpinOneChain"

using ITensors
using JLD2

function main()
    for N in [180]
        initial_states = [
            ["Up" for n=1:N],
            [
                if n <= N ÷ 2
                    "Up"
                else
                    "Dn"
                end for n=1:N
            ],
            [
                if n < N ÷ 2
                    "Up"
                elseif n == N ÷ 2
                    "Z0"
                else
                    "Dn"
                end for n=1:N
            ],
        ]
        sites = siteinds("S=1", N; conserve_qns=true)

        Jz = 1.0
        Jperp = 1.0

        hamiltonians = Dict()
        for D = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.02]
            ampo = AutoMPO()
            for j in 1:N-1
                ampo += -Jz,"Sz",j,"Sz", j+1
                ampo += -Jperp/2, "S+", j, "S-", j+1
                ampo += -Jperp/2, "S-", j, "S+", j+1
                ampo += -D, "Sz", j, "Sz", j
            end
            ampo += -D, "Sz", N, "Sz", N
            H = MPO(ampo, sites)
            hamiltonians[D] = H
        end

        sweeps_first = Sweeps(500)
        maxdim!(
            sweeps_first,
            10, 10, 10,
            20, 20, 20,
            100, 100, 100,
            200, 200, 200,
            200, 200, 200,
            500, 500, 500,
            500, 500, 500,
            500, 500, 500,
            1000)
        cutoff!(sweeps_first, 1E-1, 1E-1, 1E-3, 1E-5, 1E-10, 1E-10, 1E-10, 1E-16)

        sweeps_continue = Sweeps(500)
        maxdim!(sweeps_continue,
            500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
            500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
            1000
        )
        cutoff!(sweeps_continue, 1E-16)

        sweeps_measure = Sweeps(10)
        maxdim!(sweeps_measure, 1000)
        cutoff!(sweeps_measure, 1E-16)

        for (istate, state) in enumerate(initial_states)
            psi = productMPS(sites, state)
            sweeps = sweeps_first
            for D = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.02]
                output_filename = datadir("domainwall_N=$(N)_D=$(D)_state=$(istate).jld2")
                if isfile(output_filename)
                    @info "file $output_filename exists. skipping."
                    continue
                end

                @info "N=$N, D=$D, state: $istate"
                flush(stdout)

                H = hamiltonians[D]

                @info "dmrg-solve"
                flush(stdout)
                observer = DMRGObserver(; energy_tol=2*eps(Float64))
                energy, psi = dmrg(H, psi, sweeps; observer=observer)
                @info "done"
                sweeps = sweeps_continue

                @info "dmrg-measure"
                flush(stdout)
                observer = DMRGObserver(["Sz", "Sz2"], sites; energy_tol=2*eps(Float64))

                energy, psi = dmrg(H, psi, sweeps_measure; observer=observer)
                Szs = measurements(observer)["Sz"][end]
                Sz2s = measurements(observer)["Sz2"][end]

                @info "done"
                flush(stdout)

                @info "Saving."
                flush(stdout)
                @save(
                    output_filename,
                    {compress=true},
                    N=N,
                    D=D,
                    state=istate,
                    energy=energy,
                    Sz=Szs,
                    Sz2=Sz2s,
                )
            end
        end
    end
end

main()