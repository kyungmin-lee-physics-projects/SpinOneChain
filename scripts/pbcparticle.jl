using DrWatson
@quickactivate "SpinOneChain"

using ITensors
using JLD2

function main()
    for N in 32:16:256
        initial_states = [
            ["Up" for n in 1:N],
            [n == 1 ? "Z0" : "Up" for n in 1:N],
            [(n == 1 || n == N ÷ 2 + 1) ? "Z0" : "Up" for n in 1:N],
        ]
        sites = siteinds("S=1", N; conserve_qns=true)

        Jz = 1.0
        Jperp = 1.0

        hamiltonians = Dict()
        for D = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.09, 0.08, 0.07, 0.06, 0.05]
            ampo = AutoMPO()
            for j in 1:N-1
                ampo += -Jz,"Sz",j,"Sz", j+1
                ampo += -Jperp/2, "S+", j, "S-", j+1
                ampo += -Jperp/2, "S-", j, "S+", j+1
                ampo += -D, "Sz", j, "Sz", j
            end
            ampo += -D, "Sz", N, "Sz", N

            ampo += -Jz,"Sz", N, "Sz", 1
            ampo += -Jperp/2, "S+", N, "S-", 1
            ampo += -Jperp/2, "S-", N, "S+", 1

            H = MPO(ampo, sites)
            hamiltonians[D] = H
        end

        sweeps_first = Sweeps(500)
        maxdim!(
            sweeps_first,
            10, 10, 10,
            20, 20, 20,
            100, 100, 100,
            200, 200, 200,
            200, 200, 200,
            500, 500, 500,
            500, 500, 500,
            500, 500, 500,
            1000)
        cutoff!(sweeps_first, 1E-1, 1E-1, 1E-3, 1E-5, 1E-10, 1E-10, 1E-10, 1E-16)

        sweeps_continue = Sweeps(500)
        maxdim!(sweeps_continue,
            500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
            500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
            1000
        )
        cutoff!(sweeps_continue, 1E-16)

        sweeps_measure = Sweeps(10)
        maxdim!(sweeps_measure, 1000)
        cutoff!(sweeps_measure, 1E-16)

        for (istate, state) in enumerate(initial_states)
            psi = productMPS(sites, state)
            sweeps = sweeps_first
            for D in sort(collect(keys(hamiltonians)))
                output_filename = datadir("magnon_N=$(N)_D=$(D)_state=$(istate).jld2")
                if isfile(output_filename)
                    @info "file $output_filename exists. skipping."
                    continue
                end

                @info "N=$N, D=$D, state: $istate"
                flush(stdout)

                H = hamiltonians[D]

                @info "dmrg-solve"
                flush(stdout)
                observer = DMRGObserver(; energy_tol=2*eps(Float64))
                energy, psi = dmrg(H, psi, sweeps; observer=observer)
                @info "done"
                sweeps = sweeps_continue

                @info "dmrg-measure"
                flush(stdout)
                observer = DMRGObserver(["Sz", "Sz2"], sites; energy_tol=2*eps(Float64))

                energy, psi = dmrg(H, psi, sweeps_measure; observer=observer)
                Szs = measurements(observer)["Sz"][end]
                Sz2s = measurements(observer)["Sz2"][end]

                @info "done"
                flush(stdout)

                @info "Saving."
                flush(stdout)
                @save(
                    output_filename,
                    {compress=true},
                    N=N,
                    D=D,
                    state=istate,
                    energy=energy,
                    Sz=Szs,
                    Sz2=Sz2s,
                )
            end
        end
    end
end

main()